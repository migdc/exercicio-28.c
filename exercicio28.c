#include <stdio.h>
#include <math.h>


int permutacao(int permutacao);
int arranjosimp(int i,int h);
int arranjocrep(int a,int n);
int arranjocond(int m,int p,int m1,int p1);
int combinacao(int n,int p);

int permutacao(int permutacao)
	{
		int numero;
   		printf ("Digite um numero:\n");
		scanf ("%d", &numero);
   		permutacao=numero;
   
   		while (numero>1)
  		{	
      		permutacao=permutacao*(numero-1);
      		numero=numero-1;
   		}
   
   printf ("O valor da permutacao eh:%d\n", permutacao);
   return permutacao;
}
int arranjosimp(int i, int h)
	{
	int numero1,numero2,arra;
	printf("Digite dois numeros\n");
	scanf("%d %d",&numero1,&numero2);
	i=permutacao(numero1);
	h=permutacao(numero1-numero2);
	arra=i/h;
	printf("O valor do arranjo simples eh:%d\n",arra);
	return arra;
	} 
int arranjocrep(int a, int n)
	{
		int arcr;
		printf("Digite os dois numeros dos quais será calculado o arranjo com repeticao:\n");
		scanf("%d %d",&a,&n);
		arcr= pow(a,n);
		printf("Arranjo com Repeticao eh igual a:%d\n",arcr);
		return arcr;
	}
int arranjocond( int m, int p, int m1, int p1)
	{
		int arranjocond;
		printf("Dada a formula de Arranjo condiiconal= A(m,p)XA(m1-m,p1-p) digite os valores de m, p, m1, p1 nesta ordem:\n");
		scanf(" %d %d %d %d", &m,&p,&m1,&p1);
		arranjocond= arranjosimp(m,p)*arranjosimp(m1-m,p1-p);
		printf("O resultado eh: %d \n",arranjocond);
	}
int combinacao(int n, int p)
	{
	int comb;	
	printf("Combinar de  quanto em quanto? Digite os dois valores\n");
	scanf("%d %d", &n, &p);
	comb=(permutacao(n))/((permutacao(n-p)*permutacao(p)));
	printf(" Combinacao igual a: %d",comb);
	return comb;
	}	
int main(void)
{
        int opcao,x,y,z,w;
        printf("ESCOLHA UMA DAS OPCOES ABAIXO:\n");
        printf(" 0 - Sair\n");
        printf(" 1 - Permutacao\n");
        printf(" 2 - Arranjo Simples\n");
        printf(" 3 - Arranjo c/ repeticao\n");
        printf(" 4 - Arranjo Condicional\n");
        printf(" 5 - Combinacao");
        scanf("%d",&opcao);
        
        do
        {
            switch (opcao)
            {
                    case 0:
                            break;
                    case 1:
                            permutacao(x);
                            break;
                    case 2:
                            arranjosimp(x,y);
                            break;
                    case 3:
                            arranjocrep(x,y);
                            break;
                    case 4:
                            arranjocond(x,y,z,w);
                            break;
                    case 5:
                            combinacao(x,y);
                            break;
            }
        }while(opcao!=0)
        
        return 0;
}
